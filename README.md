# Portfolio
My motivation for this project was to demonstrate the skills I learned in class and it was a personal test to see if I am actually dedicating my time into this class.
What I built was a website and although it might not be designed that well, I did end up learning a lot when it comes to floating, css, html, and columns.
I hope to perfect this site and develop more.
I now know that this is an extremely serious course that needs more time and dedication from me. I have just signed up with a tutor and have put some stuff aside for this course from now on.
My project is still far from perfect, yet I am proud of the end result. This is just the first step of what I hope to be my future.
